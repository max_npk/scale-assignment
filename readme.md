**API Document**
https://documenter.getpostman.com/view/2335237/RzZDhwQk

**Step how to setup api**
1. Pull this repository to your local.
2. Install mongodb and run command **mongod --dbpath {path where you want to store data}**
3. Run the java application on your IDE or run **mvn install** on project directory then open /target folder and deploy .war file to tomcat server (if you want to deploy .war file to tomcat server please read 3.1).
3.1. Install Tomcat and execute **start.sh** on /bin in tomcat directory then go to **localhost:8080** and enter manager tab then deploy your .war file.
4. Now you can call a request by **localhost:8080/{endpoint}**
or **localhost:8080/{project name}/{endpoint}** if you're running this project on tomcat server.
