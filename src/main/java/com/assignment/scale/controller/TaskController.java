package com.assignment.scale.controller;

import java.util.Map;

import com.assignment.scale.model.Task;
import com.assignment.scale.service.TaskService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {
    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/api/tasks", method = RequestMethod.GET)
    public ResponseEntity<?> getAllTask() {
        return taskService.getAllTask();
    }

    @RequestMapping(value = "/api/task/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTask(@PathVariable String id) {
        return taskService.getTaskById(id);
    }

    @RequestMapping(value = "/api/task", method = RequestMethod.POST)
    public ResponseEntity<?> addTask(@RequestBody Task task) {
        return taskService.addTask(task);
    }

    @RequestMapping(value = "/api/task", method = RequestMethod.PUT)
    public ResponseEntity<?> editTask(@RequestBody Task task) {
        return taskService.editTask(task);
    }

    @RequestMapping(value = "/api/task/status", method = RequestMethod.PATCH)
    public ResponseEntity<?> changeStatus(@RequestBody Map<String, String> map) {
        return taskService.setStatus(map);
    }

    @RequestMapping(value = "/api/task", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTask(@RequestBody Map<String, String> map) {
        return taskService.deleteTask(map);
    }

    @RequestMapping(value = "/api/task/subject/{subject}", method = RequestMethod.GET)
    public ResponseEntity<?> getTaskBySubject(@PathVariable String subject) {
        return taskService.getTaskBySubject(subject);
    }
}