package com.assignment.scale.model;

import lombok.Data;

@Data
public class Task {
    private String id;
    private String subject;
    private String content;
    private String status;

    public Task(String subject, String content) {
        this.subject = subject;
        this.content = content;
    }

    public Task() {

    }
}