package com.assignment.scale.repository;

import com.assignment.scale.model.Task;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, String> {
    public Task findById(String id);

    public Task findBySubject(String subject);
}