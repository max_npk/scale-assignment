package com.assignment.scale.service;

import java.util.List;
import java.util.Map;

import com.assignment.scale.model.Task;
import com.assignment.scale.repository.TaskRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public ResponseEntity<?> getAllTask() {
        List<Task> list = (List<Task>) taskRepository.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    public ResponseEntity<?> getTaskById(String id) {
        Task task = taskRepository.findById(id);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    public ResponseEntity<?> getTaskBySubject(String subject) {
        Task task = taskRepository.findBySubject(subject);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    public ResponseEntity<?> addTask(Task task) {
        task.setStatus("PENDING");
        taskRepository.save(task);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    public ResponseEntity<?> editTask(Task task) {
        taskRepository.save(task);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<?> setStatus(Map<String, String> map) {
        Task task = taskRepository.findById(map.get("id"));
        task.setStatus("DONE");
        taskRepository.save(task);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<?> deleteTask(Map<String, String> map) {
        taskRepository.delete(map.get("id"));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}