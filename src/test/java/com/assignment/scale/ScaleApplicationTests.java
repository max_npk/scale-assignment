package com.assignment.scale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.assignment.scale.model.Task;
import com.assignment.scale.repository.TaskRepository;
import com.assignment.scale.service.TaskService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ScaleApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ScaleApplicationTests {

	@InjectMocks
	@Autowired
	private TaskService taskService;

	@Autowired
	private TaskRepository taskRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() {
		taskRepository.deleteAll();
	}

	@Test
	public void testSaveAndFind() {
		Task task1 = new Task();
		task1.setSubject("1");
		task1.setContent("1");
		task1.setStatus("PENDING");
		taskService.addTask(task1);
		Task task2 = new Task();
		task2.setSubject("2");
		task2.setContent("2");
		task2.setStatus("PENDING");
		taskRepository.save(task2);
		assertEquals(task1.getSubject(), taskRepository.findBySubject("1").getSubject());
		assertEquals(task2.getSubject(), taskRepository.findBySubject("2").getSubject());
		assertNull(taskService.getTaskBySubject("3").getBody());
	}

	@Test
	public void testStatusChange() {
		Task task = new Task();
		task.setContent("UNIT TEST");
		task.setSubject("FOR UNIT TEST");
		task.setStatus("PENDING");
		taskService.addTask(task);
		task = (Task) taskService.getTaskBySubject("FOR UNIT TEST").getBody();
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", task.getId());
		taskService.setStatus(map);
		task = taskRepository.findBySubject("FOR UNIT TEST");
		assertEquals("DONE", task.getStatus());
	}

	@Test
	public void testFindAllTasks() {
		Task task = new Task();
		task.setSubject("Test Find All");
		task.setContent("Test Find All");
		taskService.addTask(task);
		Task task2 = new Task();
		task2.setSubject("Test Find All 2");
		task2.setContent("Test Find All 2");
		taskService.addTask(task2);
		List<Task> tasks = (List<Task>) taskService.getAllTask().getBody();
		assertEquals(2, tasks.size());
	}

	@Test
	public void testEditTask() {
		Task task = new Task();
		task.setSubject("Before Edit Subject");
		task.setContent("Before Edit Content");
		taskService.addTask(task);
		Task editedTask = (Task) taskService.getTaskBySubject("Before Edit Subject").getBody();
		editedTask.setSubject("After Edit Subject");
		taskService.editTask(editedTask).getBody();
		assertNull(taskRepository.findBySubject("Before Edit Subject"));
		assertEquals(editedTask, taskRepository.findBySubject("After Edit Subject"));
	}

	@Test
	public void testDeleteTask() {
		Task task = new Task();
		task.setSubject("For test Delete");
		task.setContent("For test Delete");
		taskService.addTask(task);
		task = (Task) taskService.getTaskBySubject("For test Delete").getBody();
		Map<String, String> map = new HashMap<>();
		map.put("id", task.getId());
		taskService.deleteTask(map);
		assertNull(taskRepository.findBySubject("For test Delete"));
	}
}
